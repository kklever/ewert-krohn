﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Item;

namespace TimeLine.Modules.Client.ItemCustom
{
    public partial class wndItemCustom : wndItem
    {
		public new busArtCustom BusObj
		{
			get
			{
				return base.BusObj as busArtCustom;
			}
		}
		
		#region override WHO methods
		public int newArt = 0;
		public string artnr = String.Empty;
		
		public override void OnNew()
		{
			base.OnNew();
			artnr = Sql.TrySelectValue("SELECT ltzt_nr + 1 FROM nrkreis WHERE bel_typ = -100").ToStringNN();
			newArt = 1;
            //tbsartnr.Value = artnr;
			BusObj.NewArtikel(artnr);
		}
		
		public override int OnPostSave()
		{
			if(newArt == 1 && artnr == tbsartnr.Value.ToStringNN())
			{
				Sql.Execute("UPDATE nrkreis SET ltzt_nr = ltzt_nr + 1 WHERE bel_typ = -100");
			}
			newArt = 0;
			return base.OnPostSave();
		}
		
		#endregion
    }
}
