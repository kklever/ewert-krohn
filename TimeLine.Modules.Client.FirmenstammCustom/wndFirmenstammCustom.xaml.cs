﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.Client.Framework.NonVisual.RoutedEvents;
using TimeLine.Client.Controls;
using TimeLine.Client.Framework.Controls;

using TimeLine.Client.Controls.DX.Docking;
using TimeLine.Client.Controls.DX.Grid;

using TimeLine.Client.Controls.DX.Chart;
using TimeLine.Client.Controls.DX.Gauge;
using TimeLine.Client.Controls.DX.Pivot;

using TimeLine.Client.Controls.TX.RTF;

using TimeLine.Modules.Basis;

namespace TimeLine.Modules.Client.FirmenstammCustom
{
    public partial class wndFirmenstammCustom : wndFirmenstamm
    {
		public new BusFirmenstammCustom BusObj
		{
			get
			{
				return base.BusObj as BusFirmenstammCustom;
			}
		}
		
		public override void PostOpen()
		{
			base.PostOpen();
			var rows = xdgNrkreis.VisibleRows;
			foreach (var row in rows)
			{
				if(row["bel_typ"].ToInt(0) == -100)
				{
					row["bel_name"] = "Artikelnummer";
				}
			}
		}

		#region override WHO methods

		#endregion
    }
}
