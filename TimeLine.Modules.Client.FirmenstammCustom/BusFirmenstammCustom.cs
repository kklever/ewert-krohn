﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using TimeLine.Framework;
using TimeLine.Framework.Business;
using TimeLine.Framework.Data;
using TimeLine.Framework.Translation;
using TimeLine.Framework.Util;
using TimeLine.Framework.Util.Logging;

using TimeLine.Client.Framework;
using TimeLine.TypedDataSets;

using TimeLine.Modules.Basis;

namespace TimeLine.Modules.Client.FirmenstammCustom
{
    public class BusFirmenstammCustom : BusFirmenstamm
    {
		public new dsFirmenstammCustom tSet { get; set; }

        public BusFirmenstammCustom()
        {
			tSet = new dsFirmenstammCustom(dSet);
        }

        public override int Retrieve()
        {
            return base.Retrieve();
        }

        public override int Save()
        {
            return base.Save();
        }
    }
}
